package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class VerifierTest {

    Verifier verifier;

    @BeforeEach
    public void setVerifier(){
        verifier = new Verifier();
    }

    //Zip too short
    @Test
    public void testVerifyThrowsWhenZipIsTooShort(){

        InvalidFormatException exception = assertThrows(InvalidFormatException.class, () -> verifier.verify("321"));

        assertEquals("ERRCODE 22: INPUT_TOO_SHORT", exception.getMessage());
    }
    //Zip too long
    @Test
    public void testVerifyThrowsWhenZipIsTooLong(){

        InvalidFormatException exception = assertThrows(InvalidFormatException.class, () -> verifier.verify("2345678"));

        assertEquals("ERRCODE 21: INPUT_TOO_LONG", exception.getMessage());
    }

    //Zip out of service
    @Test
    public void testVerifyThrowsWhenZipIsOutOfService(){

        NoServiceException exception = assertThrows(NoServiceException.class, () -> verifier.verify("12234"));

        assertEquals("ERRCODE 27: NO_SERVICE", exception.getMessage());
    }
}
