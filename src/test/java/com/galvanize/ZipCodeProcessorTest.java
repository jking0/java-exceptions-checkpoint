package com.galvanize;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ZipCodeProcessorTest {

    Verifier verifier;
    ZipCodeProcessor processor;

    @BeforeEach
    public void setVerifier(){
        verifier = new Verifier();
        processor = new ZipCodeProcessor(verifier);
    }

    @Test
    public void testProcessValidMessage() {
        String actual = processor.process("80203");
        assertEquals("Thank you!  Your package will arrive soon.", actual);
    }
    @Test
    public void testProcessThrowsWhenZipIsWrongLength(){
        String actual = processor.process("321");
        assertEquals("The zip code you entered was the wrong length.", actual);

    }

    @Test
    public void testProcessThrowsWhenZipIsOutOfService() {
        String actual = processor.process("12345");
        assertEquals("We're sorry, but the zip code you entered is out of our range.", actual);
    }
}